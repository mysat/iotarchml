_Author_ = "Karthik Vaidhyanathan"

# Predictor for Spark

from keras.models import model_from_json
import keras
import pandas as pd
from Initializer import Initialize
from keras import backend as K

from sklearn.externals import joblib
import os

init_object = Initialize()


def load_model():
    # Load the model
    K.clear_session()
    json_file_co = open(init_object.model_path + 'model1_new.json', 'r')
    loaded_model_co_json = json_file_co.read()
    json_file_co.close()
    loaded_model_co = model_from_json(loaded_model_co_json)
    # load weights into new model
    loaded_model_co.load_weights(init_object.model_path + "model1_new.h5")
    print("Loaded model from disk")
    return loaded_model_co

# Load three scalars


class Spark_Predictor():
    loaded_model = None
    scalar = None
    normal = 1
    high = 0
    low = 0
    def __init__(self):
        self.loaded_model = load_model()
        self.scalar = joblib.load("scaler.save")


    def predictor(self,data_set):
        # Get the model and do predictions for the data given
        print (data_set.shape)
        K.clear_session()
        model = load_model()
        forecast = model.predict(data_set)  # Get the last 10 miniutes data for forecast
        # print (forecast)
        # inverse_forecast = forecast.reshape(forecast.shape[0] * num_forecasts, 10) # 10 denotes the number of components
        # print (forecast.shape)
        inverse_forecast = forecast.reshape(10, 13)
        inverse_forecast = self.scalar.inverse_transform(inverse_forecast)
        inverse_forecast_features = inverse_forecast.reshape(forecast.shape[0], 130)
        energy_value_actual_co = 0
        for j in range(0, inverse_forecast_features.shape[1]):
            # if j in [8, 18, 28, 38, 48, 58, 68, 78, 88, 98, 3, 13, 23, 33, 43, 53, 63, 73, 83, 93, 6, 16, 26, 36, 46, 56,
            #         66, 76, 86, 96, 7, 17, 27, 37, 47, 57, 67, 77, 87, 97]:
            #    continue
            # else:
            energy_value_actual_co = energy_value_actual_co + inverse_forecast_features[0, j]

        # Return the energy values exclude that of the componenets
        if energy_value_actual_co >= 18.0:
            print("***************************** High Energy Adaptation ***********************")
            f = open("/Users/karthik/Documents/config.txt", "w",os.O_NONBLOCK)
            f.write("s01 40000\n")
            f.write("s11 15000\n")
            f.write("s03 50000\n")
            f.write("s13 25000\n")
            f.write("s06 40000\n")
            f.write("s16 15000\n")
            f.write("s09 30000\n")
            f.write("s19 45000\n")
            f.write("s010 7000\n")
            f.write("s110 2000\n")
            f.write("s011 20000\n")
            f.write("s111 10000\n")
            f.close()

        elif energy_value_actual_co >= 14.0 and energy_value_actual_co < 17.0:
            print("***************************** Medium Energy Adaptation ***********************")
            f = open("/Users/karthik/Documents/config.txt", "w",os.O_NONBLOCK)
            f.write("s01 35000\n")
            f.write("s11 10000\n")
            f.write("s03 45000\n")
            f.write("s13 20000\n")
            f.write("s06 35000\n")
            f.write("s16 10000\n")
            f.write("s09 25000\n")
            f.write("s19 40000\n")
            f.write("s010 5000\n")
            f.write("s110 1000\n")
            f.write("s011 15000\n")
            f.write("s111 5000\n")
            f.close()

        elif energy_value_actual_co < 14.0:
            print("***************************** Low Energy Adaptation ***********************")
            f = open("/Users/karthik/Documents/config.txt", "w",os.O_NONBLOCK)
            f.write("s01 30000\n")
            f.write("s11 10000\n")
            f.write("s03 40000\n")
            f.write("s13 20000\n")
            f.write("s06 30000\n")
            f.write("s16 10000\n")
            f.write("s09 20000\n")
            f.write("s19 40000\n")
            f.write("s010 3000\n")
            f.write("s110 1000\n")
            f.write("s011 10000\n")
            f.write("s111 5000\n")
            f.close()






        return energy_value_actual_co

