_Author_ = "Karthik Vaidhyanathan"

# Script for Elastic Search Test
from elasticsearch import Elasticsearch
import traceback

class Es_Util():
    es_object = None
    def __init__(self,es_url):
        # Create an instance of elastic search for insertion
        try:
            self.es_object = Elasticsearch(es_url)
        except:
            traceback.print_exc()

    def insert(self,index,doc_type,body):
        try:
            self.es_object.index(index=index, doc_type=doc_type, body=body)
        except Exception as e:
            traceback.print_exc()




#es = Es_Util("localhost:9200")
#index = "camera1"
#doc_type = "facelogs"
#body_json = {}
#body_json["time"] = "2009-11-15T14:12:12"
#body_json["employeeId"] = "#######"
#body_json["organization"] = "Founding Minds"
#body_json["user"] = "Anish"


#es.insert(index,doc_type,body_json)