import keras
from keras.models import model_from_json
import pandas as pd
from Initializer import Initialize

from sklearn.externals import joblib
from es_utils import Es_Util
import csv

init_object = Initialize()
from datetime import datetime
import time

''''
def load_model():

    # Load the model

    json_file_co = open(init_object.model_path + 'model1_new.json', 'r')
    loaded_model_co_json = json_file_co.read()
    json_file_co.close()
    loaded_model_co = model_from_json(loaded_model_co_json)
    # load weights into new model
    loaded_model_co.load_weights(init_object.model_path + "model1_new.h5")
    print("Loaded model from disk")
    return loaded_model_co

# Load three scalars

loaded_model = load_model()

def predictor(data_set,scaler_co):
    # Get the model and do predictions for the data given
    global loaded_model
    forecast = loaded_model.predict(data_set)  # Get the last 5 miniutes data for forecast
    # print (forecast)
    # inverse_forecast = forecast.reshape(forecast.shape[0] * num_forecasts, 10) # 10 denotes the number of components
    # print (forecast.shape)
    inverse_forecast = forecast.reshape(10, 13)
    inverse_forecast = scaler_co.inverse_transform(inverse_forecast)
    inverse_forecast_features = inverse_forecast.reshape(forecast.shape[0], 130)
    energy_value_actual_co = 0
    for j in range(0, inverse_forecast_features.shape[1]):
        # if j in [8, 18, 28, 38, 48, 58, 68, 78, 88, 98, 3, 13, 23, 33, 43, 53, 63, 73, 83, 93, 6, 16, 26, 36, 46, 56,
        #         66, 76, 86, 96, 7, 17, 27, 37, 47, 57, 67, 77, 87, 97]:
        #    continue
        # else:
        energy_value_actual_co = energy_value_actual_co + inverse_forecast_features[0, j]

    # Return the energy values exclude that of the componenets
    return energy_value_actual_co


scalar = joblib.load("scaler.save")

aggregate_df= pd.read_csv("aggregated_spark.csv",sep=",",index_col="timestamp")  # Read the proccessed data frame
current_df_co = aggregate_df.iloc[1:11]
current_df_values_co = current_df_co.values
test_Set_co = current_df_values_co.reshape((1, 10, 13))
energy_value_co = predictor(test_Set_co, scalar)
print (energy_value_co)
'''

def update_text_file():
    f = open("config.txt", "w+")
    f.write("s01 30000\n")
    f.write("s11 10000\n")
    f.write("s03 40000\n")
    f.write("s13 20000\n")
    f.write("s06 30000\n")
    f.write("s16 10000\n")
    f.write("s09 20000\n")
    f.write("s19 40000\n")
    f.write("s010 3000\n")
    f.write("s110 1000\n")
    f.write("s011 10000\n")
    f.write("s111 5000\n")


def insert_to_ES():
    # To insert the data into elastic search
    elastic_object = Es_Util("localhost:9200")
    #elastic_object = Es_Util("https://elastic:7udCNDM30jBbBUV5YiTzl1qZ@cd00cb24ccc441c2a7861e66ee989afb.eu-west-1.aws.found.io:9243")
    # loop through each line of the adaptation csv file and insert into the elastic search

    with open("adaptation.csv") as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            time_string= row[0]
            datetime_object = datetime.strptime(time_string, '%Y-%m-%d %H:%M:%S')
            time_value = datetime_object.strftime("%Y-%m-%dT%H:%M:%S")
            #timestamp, S11, S13, S5, S7, S4, S12, S8, S2, S10, S3, S6, S1, S9
            body_dict = {} # To insert into Elastic Search
            body_dict["timestamp"] = time_value
            body_dict["S11"]=float(row[1])
            body_dict["S13"]=float(row[2])
            body_dict["S5"]=float(row[3])
            body_dict["S7"]=float(row[4])
            body_dict["S4"]=float(row[5])
            body_dict["S12"]=float(row[6])
            body_dict["S8"]=float(row[7])
            body_dict["S2"]=float(row[8])
            body_dict["S10"]=float(row[9])
            body_dict["S3"]=float(row[10])
            body_dict["S6"]=float(row[11])
            body_dict["S1"]=float(row[12])
            body_dict["S9"]=float(row[13])
            #print (body_dict)
            elastic_object.insert("adaptive","energy",body_dict)


        #elastic_object.insert()

def count_adaptations():
    # Count the number of adaptations made by each of the approaches
    f = open("proactive_adaptation_log_lstm.txt")
    prev_text = ""
    count_proactive = 0
    for text in f.readlines():
        #if text == prev_text:
        #    print ("dont count")
        #else:
        count_proactive += 1

        prev_text=text


    f = open("reactive_adaptation_log.txt")
    prev_text_react = ""
    count_reactive = 0
    for text in f.readlines():
        if text == prev_text_react:
            print("dont count")
        else:
            count_reactive += 1

        prev_text_react = text
    print(count_proactive)
    print(count_reactive)



if __name__ == '__main__':
    #update_text_file()
    #insert_to_ES()
    count_adaptations()