_Author_ = "Karthik Vaidhyanathan"
# This class is responsible for genearating the plots of energy simulations based on the data from CupCarbon


from configparser import ConfigParser

import json

import csv
import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import traceback
from Initializer import Initialize
import numpy as np
from pandas import Series
import matplotlib.style as style
style.use('seaborn-poster') #sets the size of the charts
style.use('ggplot')
import time


init_object = Initialize() # Create the object for loading the configurations

class Plot_Generator():
    # This class contains the functions for generating different types of plot as per the requirements

    def __init__(self):

        self.data_path = init_object.data_path
        self.json_path = init_object.json_path
        self.energy_val = init_object.energy_val
        self.component_count = int(init_object.component_count)


    def generate_plot(self):
        # Function to create a multi line plot
        dataframe = None
        try:
            dataframe = pd.read_csv(self.data_path + "processed_data.csv",sep=",") # Read the proccessed data frame

        except Exception as e:
            traceback.print_exc()

        # Create the plot
        plt.plot('timestamp', 'S1', data=dataframe, marker='', color='skyblue', linewidth=1,label="S1")
        plt.plot('timestamp', 'S2', data=dataframe, marker='', color='olive', linewidth=1,label="S2")
        plt.plot('timestamp', 'S3', data=dataframe, marker='', color='red', linewidth=1,label="S3")
        plt.plot('timestamp', 'S4', data=dataframe, marker='', color='pink', linewidth=1,label="S4")
        plt.plot('timestamp', 'S5', data=dataframe, marker='', color='blue', linewidth=1,label="S5")
        plt.plot('timestamp', 'S6', data=dataframe, marker='', color='violet', linewidth=1,label="S6")
        #plt.plot('timestamp', 'S3', data=dataframe, marker='', color='olive', linewidth=1, linestyle='dashed', label="S3")
        plt.legend()
        plt.show()



    def generate_plot_aggregation(self):
        # Gets the aggregated data and then plots it
        dataframe = None
        try:
            dataframe = pd.read_csv(self.data_path + "aggregated_data_SynthUtilize.csv",sep=",",index_col="timestamp") # Read the proccessed data frame
            print (dataframe)
            #dataframe.plot(subplots=True)
            plt.plot(dataframe["S8"].values)
            plt.show()
            plt.savefig("plot_aggre.png")
        except Exception as e:
            traceback.print_exc()



    def generate_total_energy_plot(self):
        # Create the energy plots for the total energy consumed for each of the values

        collect_org_adapted_proactive = []
        collect_org_original = []
        collect_org_adapted_reactive = []



        # For normal

        df_collect_org_normal = pd.read_csv(
            "/Users/karthik/PycharmProjects/CAPS_JSS/data/aggregated_1day_low.csv", sep=",",
            index_col="timestamp")  # Read the proccessed data frame
        df_collect_series = df_collect_org_normal.values
        # print(df_collect_org)
        #print(type(df_collect_series))
        prev_vals = [19160.0, 19160.0, 19160.0, 19160.0, 19160.0, 19160.0, 19160.0, 19160.0, 19160.0, 19160.0, 19160.0,
                     19160.0, 19160.0]
        # prev_vals = [19160.0,19160.0,19160.0,19160.0,19160.0,19160.0,19160.0,19160.0,19160.0,19160.0]

        # Total Sum
        #for i in range(0, df_collect_series.shape[0]):
        for i in range(1200,1440):
            energy_value = 0
            for j in range(0, 13):
                #if (j==8 or j==3 or j==6 or j==7):
                #    continue
                #else:
                energy_j = prev_vals[j] - df_collect_series[i, j]
                energy_value = energy_value + energy_j
                prev_vals[j] = energy_j

            collect_org_original.append(energy_value)


        df_collect_org_adapted_proactive = pd.read_csv("/Users/karthik/PycharmProjects/CAPS_JSS/adaptation.csv",
                                                       sep=",",header=None)  # Read the proccessed data frame
        df_collect_org_adapted_proactive.columns = ["timestamp", "S11", "S13", "S5", "S7", "S4", "S12", "S8", "S2",
                                                    "S10", "S3", "S6", "S1", "S9"]
        df_collect_org_adapted_proactive.set_index("timestamp",inplace=True)


        print(df_collect_org_adapted_proactive)
        df_collect_series = df_collect_org_adapted_proactive.values
        # print(df_collect_org)
        print(type(df_collect_series))
        prev_vals = [19160.0, 19160.0, 19160.0, 19160.0, 19160.0, 19160.0, 19160.0, 19160.0, 19160.0, 19160.0,
                     19160.0, 19160.0, 19160.0]
        # prev_vals = [19160.0,19160.0,19160.0,19160.0,19160.0,19160.0,19160.0,19160.0,19160.0,19160.0]

        # Total Sum
        for i in range(1200, 1440):
            energy_value = 0
            for j in range(0, 13):
                #if (j==8 or j==3 or j==6 or j==7):
                #    continue
                #else:
                energy_j = prev_vals[j] - df_collect_series[i, j]
                energy_value = energy_value + energy_j
                prev_vals[j] = energy_j

            collect_org_adapted_proactive.append(energy_value)

        df_collect_org_adapted_reactive = pd.read_csv("/Users/karthik/PycharmProjects/CAPS_JSS/adaptation_reactive.csv",
                                                       sep=",", header=None)  # Read the proccessed data frame
        df_collect_org_adapted_reactive.columns = ["timestamp", "S11", "S13", "S5", "S7", "S4", "S12", "S8", "S2",
                                                    "S10", "S3", "S6", "S1", "S9"]
        df_collect_org_adapted_reactive.set_index("timestamp", inplace=True)

        print(df_collect_org_adapted_reactive)
        df_collect_series = df_collect_org_adapted_reactive.values
        # print(df_collect_org)
        print(type(df_collect_series))
        prev_vals = [19160.0, 19160.0, 19160.0, 19160.0, 19160.0, 19160.0, 19160.0, 19160.0, 19160.0, 19160.0,
                     19160.0, 19160.0, 19160.0]
        # prev_vals = [19160.0,19160.0,19160.0,19160.0,19160.0,19160.0,19160.0,19160.0,19160.0,19160.0]

        # Total Sum
        for i in range(1200, 1440):
            energy_value = 0
            for j in range(0, 13):
                #if (j==8 or j==3 or j==6 or j==7):
                #    continue
                #else:
                energy_j = prev_vals[j] - df_collect_series[i, j]
                energy_value = energy_value + energy_j
                prev_vals[j] = energy_j

            collect_org_adapted_reactive.append(energy_value)

        plt.plot(collect_org_adapted_proactive, label="adapted")
        plt.plot(collect_org_original,label = "normal")
        plt.plot(collect_org_adapted_reactive,label = "reactive")

        #df_collect_org.plot(subplots=True)
        plt.legend()

        plt.savefig("adaptation_graph.png")
        #plt.show()
        print (max(collect_org_original)-min(collect_org_original))
        print (max(collect_org_adapted_proactive)-min(collect_org_adapted_proactive))
        print (max(collect_org_adapted_reactive)-min(collect_org_adapted_reactive))



    def generate_bar_plot(self):
        plt.rcdefaults()
        objects = ('Normal', 'Reactive', 'Proactive LSTM', 'Proactive CNN', 'Proactive ARIMA')

        y_pos = np.arange(len(objects))
        performance = [3039.93,2340.55,1962.96,2391.13,2232.03]
        ind = np.arange(len(performance))
        width = 0.35  # the width of the bars


        plt.xticks(rotation='45')

        plt.rc('font', family='serif', serif='Times')
        plt.rc('text', usetex=True)
        plt.rc('xtick', labelsize=8)
        plt.rc('ytick', labelsize=8)
        plt.rc('axes', labelsize=8)

        width = 5.0
        height = width / 1.618

        fig, ax = plt.subplots()
        fig.subplots_adjust(left=.15, bottom=.16, right=.99, top=.97)

        plt.bar(y_pos, performance, align='center', alpha=0.5,color=(0.2, 0.4, 0.6, 0.6))
        for i, cty in enumerate(performance):
            ax.text(i, cty + 1, round(cty, 1), horizontalalignment='center')

        plt.xticks(y_pos, objects)
        ax.set_ylabel('Energy Consumption (Joules)')
        #ax.set_xlabel('Time (Minutes)')
        fig.set_size_inches(width, height)

        plt.savefig("bargraph_adaptation.pdf")


    #def total_energy_plot_one(self):

    def generate_pie_plot(self):
        # This generates the pie plot for each of the three approaches
        labels = ['S11','S13','S5','S7','S4','S12','S8','S2','S10','S3','S6','S1','S9']

        pie_dict = {}
        for keys in labels:
            pie_dict[keys] = []
        with open("/Users/karthik/PycharmProjects/CAPS_JSS/data/aggregated_1day_low.csv") as csvfile:
            csvreader = csv.reader(csvfile, delimiter=',' )
            count = 0
            for row in csvreader:
                if count>0:
                    pie_dict["S11"].append(float(row[1]))
                    pie_dict["S13"].append(float(row[2]))
                    pie_dict["S5"].append(float(row[3]))
                    pie_dict["S7"].append(float(row[4]))
                    pie_dict["S4"].append(float(row[5]))
                    pie_dict["S12"].append(float(row[6]))
                    pie_dict["S8"].append(float(row[7]))
                    pie_dict["S2"].append(float(row[8]))
                    pie_dict["S10"].append(float(row[9]))
                    pie_dict["S3"].append(float(row[10]))
                    pie_dict["S6"].append(float(row[11]))
                    pie_dict["S1"].append(float(row[12]))
                    pie_dict["S9"].append(float(row[13]))

                count+=1


        sizes = []
        for keys in pie_dict:
            sizes.append(sum(pie_dict[keys]))

        sizes[3] = 18.5
        sizes[10] = sizes[10]-sizes[3]
        #print (sizes)
        # only "explode" the 2nd slice (i.e. 'Hogs')
        #explode = (0, 0.1, 0, 0)
        ''''
        fig1, ax1 = plt.subplots()
        ax1.pie(sizes, labels=labels, autopct='%1.1f%%',
                shadow=True, startangle=90)
        # Equal aspect ratio ensures that pie is drawn as a circle
        ax1.axis('equal')
        plt.tight_layout()
        plt.savefig("pie_reactive.png")
        
        plt.xticks(rotation='45')
        '''
        y_pos = np.arange(len(labels))

        plt.rc('font', family='serif', serif='Times')
        plt.rc('text', usetex=True)
        plt.rc('xtick', labelsize=8)
        plt.rc('ytick', labelsize=8)
        plt.rc('axes', labelsize=8)

        width = 5.0
        height = width / 1.618

        fig, ax = plt.subplots()
        fig.subplots_adjust(left=.15, bottom=.16, right=.99, top=.97)

        plt.bar(y_pos, sizes, align='center', alpha=0.5, color=(0.6, 0.6, 0.6, 0.6))
        for i, cty in enumerate(sizes):
            ax.text(i, cty + 1, round(cty, 1), horizontalalignment='center')

        plt.xticks(y_pos, labels)
        ax.set_ylabel('Energy Consumption (Joules)')
        # ax.set_xlabel('Time (Minutes)')
        fig.set_size_inches(width, height)

        plt.savefig("bargraph_effective_normal.pdf")


plot = Plot_Generator()
#plot.generate_plot()
#plot.generate_plot_aggregation()
plot.generate_total_energy_plot()
#plot.generate_bar_plot()
#plot.generate_pie_plot()