import os
#os.environ['PYSPARK_SUBMIT_ARGS'] = '--packages org.apache.spark:spark-streaming-kafka-0-8_2.11:2.0.2 pyspark-shell'
#    Spark
from pyspark import SparkContext,SparkConf
#    Spark Streaming
from pyspark.streaming import StreamingContext
#    Kafka
from pyspark.streaming.kafka import KafkaUtils
#    json parsing
import json
import time
#import sqlUtils
from elasticsearch import Elasticsearch
from datetime import timedelta
from Initializer import Initialize

import pandas as pd
from datetime import datetime

from Spark_Predictor import Spark_Predictor
start_time = datetime.now()
prev_vals = [19160.0, 19160.0, 19160.0, 19160.0, 19160.0, 19160.0, 19160.0, 19160.0, 19160.0, 19160.0, 19160.0,
                     19160.0, 19160.0]   # Initialize the inital energy configuration

last_read = 0
#sql_connector = sqlUtils.SqlConnector()   # SQL util class

es = Elasticsearch() # Connect to the default ES
init_object = Initialize()
spark_predictor = Spark_Predictor()

main_df = pd.DataFrame(columns=["timestamp","S11","S13","S5","S7","S4","S12","S8","S2","S10","S3","S6","S1","S9"])

def ingest(rdd):
    index = 0
    global  start_time
    global prev_vals
    global main_df
    global last_read
    # for taking the stream of data and processing it
    rows = rdd.map(lambda line: line.split(";"))
    dict_map = {}
    dict_map["timestamp"] = []
    dict_map["S11"] = []
    dict_map["S13"] = []
    dict_map["S5"] = []
    dict_map["S7"] = []
    dict_map["S4"] = []
    dict_map["S12"] = []
    dict_map["S8"] = []
    dict_map["S2"] = []
    dict_map["S10"] = []
    dict_map["S3"] = []
    dict_map["S6"] = []
    dict_map["S1"] = []
    dict_map["S9"] = []
    for row in rows.take(rows.count()):
        #print(row[0])
        #print (row[1])
        data_frame_data = []
        time_string = row[0]
        second_level_data = []
        time_value = start_time + timedelta(milliseconds=float(row[0]) * 1000)
        #print (float(row[0]))
        timestamp = int(time_value.timestamp() * 1000)
        #print(timestamp)
        dict_map["timestamp"].append(timestamp)
        # insert to data_frame data so that it can go separately as column of the dataframe
        data_frame_data.append(timestamp)
        for index in range(1,len(prev_vals)+1):
            instant_data = prev_vals[index-1]-float(row[index])

            #data_frame_data.append(instant_data)
            dict_map["S"+ str(index)].append(instant_data)
            prev_vals[index-1] = float(row[index])


            # Elastic insert
            time_string = row[0]

            datetime_object = datetime.strptime(time_string, '%Y-%m-%d %H:%M:%S')
            time_value = datetime_object.strftime("%Y-%m-%dT%H:%M:%S")
            # timestamp, S11, S13, S5, S7, S4, S12, S8, S2, S10, S3, S6, S1, S9
            body_dict = {}  # To insert into Elastic Search
            body_dict["timestamp"] = time_value
            body_dict["S11"] = float(row[1])
            body_dict["S13"] = float(row[2])
            body_dict["S5"] = float(row[3])
            body_dict["S7"] = float(row[4])
            body_dict["S4"] = float(row[5])
            body_dict["S12"] = float(row[6])
            body_dict["S8"] = float(row[7])
            body_dict["S2"] = float(row[8])
            body_dict["S10"] = float(row[9])
            body_dict["S3"] = float(row[10])
            body_dict["S6"] = float(row[11])
            body_dict["S1"] = float(row[12])
            body_dict["S9"] = float(row[13])
            # print (body_dict)
            es.index("adaptive", "energy", body_dict)


        #df.iloc[index] = data_frame_data
        #formatted_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(time_string)))

        #time_value_string = datetime.fromtimestamp(int(timestamp)).strftime("%Y-%m-%dT%H:%M:%S")
        #print (time_value_string)
        #datetime_object = datetime.strptime(time_string,"%Y-%m-%d %H:%M:%S")

        #sql = "insert into sensor_values values(" + "\"" + sensodId + "\"," + "\"" + str(time_value) + "\"," + str(value) + ");"

        #query_string = sql.replace("$SID",\"sensorId\").replace("$TIME",str(time_value)).replace("$VALUE",str(value))
        #print (sql)
        #sql_connector.insert(sql)
        #doc = {
        #    'sensorId': sensodId,
        #    '@timestamp': time_string,
        #    'value': value,
        #}
        #res = es.index(index="sensor1", doc_type='data', body=doc)
    #print(dict_map)
    data_frame = pd.DataFrame(dict_map)
    main_df = main_df.append(data_frame,ignore_index=True)

    ds = pd.to_datetime(main_df["timestamp"],
                        unit='ms')  # Convert the timestamps to new value to get the aggregated time stamp

    column_list = main_df.columns.values
    process_df = main_df[[column_list[1]]].copy()  # Ignore the timestamp column
    for data in column_list:
        # Loop through all the columns and keep adding them
        if data != "timestamp":
            process_df[data] = main_df[data].tolist()
    process_df.index = ds
    aggregate_df = process_df.resample('1T').sum()
    aggregate_df.to_csv("aggregated_spark.csv", index=True)

    if len(aggregate_df)>=10:
        print (aggregate_df)
        aggregate_df= pd.read_csv("aggregated_spark.csv",sep=",",index_col="timestamp")  # Read the proccessed data frame
        current_df_co = aggregate_df.iloc[0:10]
        current_df_values_co = current_df_co.values
        test_Set_co = current_df_values_co.reshape((1, 10, 13))
        energy_value_co = spark_predictor.predictor(test_Set_co)
        print (energy_value_co)
        # Drop all rows of main_df
        main_df = pd.DataFrame(columns=["timestamp", "S11", "S13", "S5", "S7", "S4", "S12", "S8", "S2", "S10", "S3", "S6", "S1", "S9"])



    #print "here"

os.environ['PYSPARK_SUBMIT_ARGS'] = '--jars spark-streaming-kafka-assembly_2.10-1.6.0.jar pyspark-shell'

#batchIntervalSeconds = 10

sc = SparkContext(appName="PythonSparkStreamingKafka_RM_01")
sc.setLogLevel("WARN")
ssc = StreamingContext(sc, 10)


kafkaStream = KafkaUtils.createStream(ssc, 'localhost:2181', 'spark-streaming', {'sensor':1})
#lines = kafkaStream.map(lambda line: line.split(" "))

lines = kafkaStream.map(lambda x: x[1])
line = lines.foreachRDD(lambda rdd : ingest(rdd))




ssc.start()
ssc.awaitTermination()


''''
def creatingFunc():
    ssc = StreamingContext(sc, batchIntervalSeconds)
    # Set each DStreams in this context to remember RDDs it generated in the last given duration.
    # DStreams remember RDDs only for a limited duration of time and releases them for garbage
    # collection. This method allows the developer to specify how long to remember the RDDs (
    # if the developer wishes to query old data outside the DStream computation).
    ssc.remember(60)

if __name__ == '__main__':
    creatingFunc()

'''