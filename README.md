# IoTArchML

Implementation of IoTArchML, an approach that allows IoT architectures to learn and improve continuously using machine learning techniques with respect to QoS parameters such that the overall performance is not compromised

## Project Description
1. CupCarbon-master contains the modified source code of cupcarbon. The application can be started by running cupcarbon.java
2. NdR\_Cup contains the cupCarbon project of the case study mentioned in the paper. It can be opened by opening the NdR_Cup.cup filefrom the
open project option available in the cupCarbon UI. Further details can be found in [www.cupcarbon.com](url)
3. The data folder contains the different datasets used for experimentation and evaluations
4. model folder contains the machine learning models developed using Keras for energy and data traffi consumption. 
5. settings.conf contains the inital configurations required for all programs and this is inturn read and processed by Initalizer.py

## Installtion Requirements
1. Install Apache Kafka  - [https://kafka.apache.org/quickstart](url)
2. Install kafka-python - [https://pypi.org/project/kafka-python/](url)
3. Install Keras in Python -[https://keras.io](url)
4. Install Apache Spark - [https://intellipaat.com/tutorial/spark-tutorial/downloading-spark-and-getting-started/](url)
5. Install PySpark - [https://medium.com/@GalarnykMichael/install-spark-on-ubuntu-pyspark-231c45677de0](url)